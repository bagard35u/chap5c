PGM     = liste
OBJS    = liste.o main.o

CC      = gcc
CFLAGS  = -c -Wall -ansi -pedantic
LDFLAGS = 

all: $(PGM)

$(PGM): $(OBJS)
	$(CC) -o $(PGM) $(OBJS) $(LDFLAGS)

.c.o:
	$(CC) $(CFLAGS) $<

clean:
	rm -f $(PGM)
	rm -f $(OBJS)
	rm -f *~
